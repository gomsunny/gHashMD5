object vMain: TvMain
  Left = 0
  Top = 0
  Caption = 'Calc MD5'
  ClientHeight = 134
  ClientWidth = 333
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListInfo: TValueListEditor
    Left = 0
    Top = 31
    Width = 333
    Height = 103
    Align = alClient
    BorderStyle = bsNone
    Color = clWhite
    DoubleBuffered = False
    FixedCols = 1
    GradientEndColor = clBtnFace
    GradientStartColor = clBtnFace
    ParentDoubleBuffered = False
    Strings.Strings = (
      'Path='
      'FileName='
      'Size='
      'MD5=')
    TabOrder = 0
    TitleCaptions.Strings = (
      'App version'
      '1.0.0.0')
    OnEditButtonClick = ListInfoEditButtonClick
    ExplicitTop = 25
    ExplicitWidth = 478
    ExplicitHeight = 101
    ColWidths = (
      68
      263)
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 327
    Height = 25
    Align = alTop
    BevelKind = bkSoft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 478
    object LabelFile: TLabel
      Left = 0
      Top = 0
      Width = 323
      Height = 21
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 'Drag and drop the file which to calculate MD5'
      EllipsisPosition = epPathEllipsis
      Layout = tlCenter
      ExplicitLeft = 3
      ExplicitTop = 3
      ExplicitWidth = 352
      ExplicitHeight = 22
    end
  end
  object RzVersionInfo1: TRzVersionInfo
    Left = 184
    Top = 32
  end
end
