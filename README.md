# gHashMD5

Download: [gHashMD5.exe](./release/gHashMD5.exe)

![screen shot](gHashMD5.png)

The gHashMD5 is a simple tool for calculating file's MD5 hash.

## How to use

1. Drag the file into the gMD5Hash.
2. For copy a text on the row, just clicking the row or right button.

gHashMD5는 파일의 MD5 해쉬 값을 구할 수 있는 간단한 계산기 입니다.

## 사용방법

1. 파일을 gHashMD5에 드래그 후 드롭 하세요.
2. 각 행의 복사는 행을 클릭하거나 버튼을 누르면 됩니다.